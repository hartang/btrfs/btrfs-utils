#!/usr/bin/env bash
#
# Install systemd units to their correct location.
set -euo pipefail

# Units to install
UNITS=( "bees@.service" "btrbk@.service" "btrbk@.timer" "machine-bees.slice"
        "btrfs-balance@.timer" "btrfs-balance@.service" )
# Target folder to install units to
# Original location was '/usr/local/lib/systemd/system', but that doesn't work
# on rpm-ostree (See: https://github.com/coreos/rpm-ostree/issues/1936)
TARGET_DIR="/etc/systemd/system/"
# Required utilities for this script
REQUIREMENTS=( "systemctl" )
if command -v "getenforce" &>/dev/null && [[ "$(getenforce)" == "Enforcing" ]]; then
    REQUIREMENTS+=("restorecon")
fi

function _fatal {
    echo "ERROR: $*" 1>&2
    exit 1
}

# Check for pre-requirements
[[ "$EUID" -eq 0 ]] || _fatal "this script must be run as root"
for r in "${REQUIREMENTS[@]}"; do
    command -v "$r" &>/dev/null || _fatal "command '$r' is required but not available"
done
[[ -d "$TARGET_DIR" ]] || \
    mkdir -p "$TARGET_DIR" || \
    _fatal "cannot create target folder at '$TARGET_DIR', aborting"

# Install the files
for u in "${UNITS[@]}"; do
    install -b -C -g root -o root -m 644 -Z "$u" "$TARGET_DIR/$u"
    echo "'$u' installed"
done
systemctl daemon-reload

echo ""
echo "Installation successful."
echo "You may want to restart any running units now."
