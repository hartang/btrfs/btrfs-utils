# Utilities for working with Btrfs filesystems

My personal collection of systemd-units, scripts and useful commands for
working with Btrfs, tried and tested on my personal collection of computers.
